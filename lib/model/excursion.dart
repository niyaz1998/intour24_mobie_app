class Excursion {
  String id,
      title,
      startTime,
      endTime,
      duration,
      cost,
      services,
      photoLink,
      titleOriginal,
      type,
      language,
      description,
      departurePlace,
      destination,
      ticketPriceAdult,
      ticketPriceChild;

  List<String> excursionDates;

  List<String> photosLink;

  Excursion(
      {this.id,
      this.title,
      this.startTime,
      this.endTime,
      this.duration,
      this.cost,
      this.services,
      this.photoLink,
      this.titleOriginal,
      this.type,
      this.language,
      this.description,
      this.departurePlace,
      this.destination,
      this.ticketPriceAdult,
      this.ticketPriceChild,
      this.excursionDates,
      this.photosLink});

  /// returns list of code for services provided by excursion
  List<String> getCodes() {
    return services.split('.');
  }

  /// returns price for adults
  double getAdultPrice() {
    var split = ticketPriceAdult.split(' ');
    return double.parse(split[0]);
  }

  /// return price for children
  double getChildPrice() {
    var split = ticketPriceChild.split(' ');
    return double.parse(split[0]);
  }

  /// currency for prices (adult and children prices)
  /// for now assumed that currency is equal for adult and children
  String getCurrency() {
    var split = ticketPriceAdult.split(' ');
    return split[1];
  }
}


/// represents purchased excursion
class PurchasedExcursion {
  /// what excursion was purchased
  Excursion excursion;
  /// amount of tickets for adults and children
  int adultTickets, childTickets;
  /// for which date excursion was purchased
  String date;

  PurchasedExcursion(
      this.excursion, this.adultTickets, this.childTickets, this.date);
}

final List<Excursion> excursionPreList = <Excursion>[
  Excursion(
      id: '1',
      title: 'Музей имени Андрея с котиками',
      startTime: '12:00',
      endTime: '13:00',
      duration: '1 hour',
      cost: '100 руб',
      services: 'HUMN.BIKE',
      photoLink:
          'https://avatars.mds.yandex.net/get-pdb/931085/b0a9d509-a5cf-4b84-a9ed-40e8fc5c6ab3/s1200?webp=false',
      titleOriginal: 'Музей имени Андрея с котиками',
      type: 'HSTR',
      language: 'русишь',
      description: 'some original description это обычный текст '
          'это обычный текст это обычный текст это обычный текст '
          'это обычный текст это обычный текст это обычный текст '
          'это обычный текст это обычный текст это обычный текст '
          'это обычный текст это обычный текст это обычный текст '
          'это обычный текст это обычный текст это обычный текст '
          'это обычный текст это обычный текст это обычный текст '
          'это обычный текст это обычный текст это обычный текст '
          'это обычный текст это обычный текст это обычный текст '
          'это обычный текст это обычный текст это обычный текст '
          'это обычный текст это обычный текст это обычный текст '
          'это обычный текст это обычный текст это обычный текст '
          'это обычный текст это обычный текст это обычный текст '
          'это обычный текст это обычный текст это обычный текст '
          'это обычный текст это обычный текст это обычный текст '
          'это обычный текст это обычный текст это обычный текст '
          'это обычный текст это обычный текст это обычный текст '
          'это обычный текст это обычный текст это обычный текст ',
      departurePlace: 'ул. ОфигенныхПацанов',
      destination: 'ул. ЧеткихПацанов',
      ticketPriceAdult: '100 руб',
      ticketPriceChild: '200 руб',
      excursionDates: [
        '12.01.2019',
        '14.01.2019',
        '16.01.2019'
      ],
      photosLink: [
        'https://f3.upet.com/K_mjd3D2Vq1q_1.jpg',
        'https://funik.ru/wp-content/uploads/2018/10/cd7412da1e03af2ad39e.jpg',
        'https://avatars.mds.yandex.net/get-pdb/222892/3ecee8de-e6a9-4f1f-91c5-ea41cce169a1/s1200?webp=false'
      ]),
  Excursion(
      id: '0',
      title: 'some title 0',
      startTime: '12:00',
      endTime: '13:00',
      duration: '1 hour',
      cost: '100 руб',
      services: 'HUMN.BIKE.TICT.ICEC.MASK',
      photoLink:
          'https://vistanews.ru/uploads/posts/2018-03/1522263543_2fons.ru-82736.jpg',
      titleOriginal: 'some long title 0',
      type: 'FOOD',
      language: 'русишь',
      description: 'some original description',
      departurePlace: 'ул. ОфигенныхПацанов',
      destination: 'ул. ЧеткихПацанов',
      ticketPriceAdult: '100 руб',
      ticketPriceChild: '200 руб',
      excursionDates: [
        '12.01.2019',
        '13.01.2019',
        '15.01.2019'
      ],
      photosLink: [
        'http://ru.hdwall365.com/wallpapers/1608/Cute-puppy-in-grass-golden-retriever_1920x1200_wallpaper.jpg',
        'https://www.sunhome.ru/i/wallpapers/44/schenok-s-rozoi.orig.jpg'
      ]),
];

final List<PurchasedExcursion> purchasedExcursion = [
  PurchasedExcursion(
      excursionPreList[0], 2, 3, excursionPreList[0].excursionDates[0]),
  PurchasedExcursion(
      excursionPreList[0], 3, 0, excursionPreList[0].excursionDates[1]),
  PurchasedExcursion(
      excursionPreList[1], 4, 1, excursionPreList[1].excursionDates[2]),
];
