class Hotel {

  String _title, _address, _room_code, _catering_code, _vouchers_link;

  Hotel(this._title, this._address, this._room_code, this._catering_code,
      this._vouchers_link);

  get vouchers_link => _vouchers_link;

  set vouchers_link(value) {
    _vouchers_link = value;
  }

  get catering_code => _catering_code;

  set catering_code(value) {
    _catering_code = value;
  }

  get room_code => _room_code;

  set room_code(value) {
    _room_code = value;
  }

  get address => _address;

  set address(value) {
    _address = value;
  }

  String get title => _title;

  set title(String value) {
    _title = value;
  }


}