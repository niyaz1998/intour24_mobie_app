
class QuestionsTopic {
  final String topic;
  final List<QuestionEntry> questions;

  QuestionsTopic(this.topic, this.questions);
}

class QuestionEntry {
  QuestionEntry(this.question, this.answer);

  final String question;
  final String answer;
}


final QuestionsTopic data = QuestionsTopic('topic AAA', <QuestionEntry>[
  QuestionEntry('question A', 'answer B'),
  QuestionEntry('question B', 'answer A'),
  QuestionEntry('question C', 'answer XX'),
]);



final List<QuestionsTopic> mainData = [
  QuestionsTopic('topic AAA', <QuestionEntry>[
    QuestionEntry('question A', 'answer B'),
    QuestionEntry('question B', 'answer A'),
    QuestionEntry('question C', 'answer XX'),
  ]),
  QuestionsTopic('topic XXX', <QuestionEntry>[
    QuestionEntry('question X', 'answer Y'),
    QuestionEntry('question Z', 'answer XY'),
    QuestionEntry('question A', 'answer B'),
  ]),
  QuestionsTopic('topic a', <QuestionEntry>[
    QuestionEntry('question a', 'answer a'),
    QuestionEntry('question b', 'answer b'),
    QuestionEntry('question c', 'answer c'),
  ]),
];
