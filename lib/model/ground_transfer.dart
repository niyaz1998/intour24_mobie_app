class GroundTransfer {

  String _type, _datetime, _departure_place, _destination;


  GroundTransfer(this._type, this._datetime, this._departure_place,
      this._destination);

  String get type => _type;

  set type(String value) {
    _type = value;
  }

  get datetime => _datetime;

  get destination => _destination;

  set destination(value) {
    _destination = value;
  }

  get departure_place => _departure_place;

  set departure_place(value) {
    _departure_place = value;
  }

  set datetime(value) {
    _datetime = value;
  }


}