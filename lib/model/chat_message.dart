class ChatMessage {
  // content of the message
  String text;
  // when message was received on server
  DateTime time;
  // who is sender of the message
  MessageSender sender;
  // if sender is a user then status of message is considered
  MessageStatus status;

  ChatMessage(this.text, this.time, this.sender, this.status);
}

enum MessageSender { user, agent }

enum MessageStatus { sending, sent }



List<ChatMessage> data = [
  ChatMessage('Здравствуйте помогите мне решить задачку по математике',
      DateTime(2018, 11, 10, 12, 10), MessageSender.user, MessageStatus.sent),
  ChatMessage(
      'Конечно! с удовольствием, чем я могу помочь?',
      DateTime(2018, 11, 10, 12, 15),
      MessageSender.agent,
      MessageStatus.sent),
  ChatMessage('Сколько весит килограмм яблок?',
      DateTime(2018, 11, 10, 12, 16), MessageSender.user, MessageStatus.sent),
  ChatMessage(
      'Ваш вопрос очень важен для нас, я уже вызвал наших специалистов для решения вашей проблемы',
      DateTime(2018, 11, 10, 12, 17),
      MessageSender.agent,
      MessageStatus.sent),
  ChatMessage(
      'С нетерпением жду вашего ответа',
      DateTime(2018, 11, 10, 12, 16),
      MessageSender.user,
      MessageStatus.sending)
];