
class Tourist {
  String _firstname, _lastname, _gender, _birthdate, _passport, _citizenship;


  Tourist(this._firstname, this._lastname, this._gender, this._birthdate,
      this._passport, this._citizenship);

  String get firstname => _firstname;

  set firstname(String value) {
    _firstname = value;
  }

  get lastname => _lastname;

  get citizenship => _citizenship;

  set citizenship(value) {
    _citizenship = value;
  }

  get passport => _passport;

  set passport(value) {
    _passport = value;
  }

  get birthdate => _birthdate;

  set birthdate(value) {
    _birthdate = value;
  }

  get gender => _gender;

  set gender(value) {
    _gender = value;
  }

  set lastname(value) {
    _lastname = value;
  }


}