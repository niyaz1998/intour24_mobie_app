
class Flight {

  String _firstname, _lastname, _ticket_number,
  _ticket_class, _luggage, _destination, _flight_datetime,
  _airport, _online_register, _tickets_link;


  Flight(this._firstname, this._lastname, this._ticket_number,
      this._ticket_class, this._luggage, this._destination,
      this._flight_datetime, this._airport, this._online_register,
      this._tickets_link);

  String get firstname => _firstname;

  set firstname(String value) {
    _firstname = value;
  }

  get lastname => _lastname;

  get tickets_link => _tickets_link;

  set tickets_link(value) {
    _tickets_link = value;
  }

  get online_register => _online_register;

  set online_register(value) {
    _online_register = value;
  }

  get airport => _airport;

  set airport(value) {
    _airport = value;
  }

  get flight_datetime => _flight_datetime;

  set flight_datetime(value) {
    _flight_datetime = value;
  }

  get destination => _destination;

  set destination(value) {
    _destination = value;
  }

  get luggage => _luggage;

  set luggage(value) {
    _luggage = value;
  }

  get ticket_class => _ticket_class;

  set ticket_class(value) {
    _ticket_class = value;
  }

  get ticket_number => _ticket_number;

  set ticket_number(value) {
    _ticket_number = value;
  }

  set lastname(value) {
    _lastname = value;
  }


}