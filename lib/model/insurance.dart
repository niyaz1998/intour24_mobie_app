
class Insurance {

  String _company, _start_date, _end_date, _payment, _firstname, _lastname, _insurance_number,
  _company_phone, _polis_link;


  Insurance(this._company, this._start_date, this._end_date, this._payment,
      this._firstname, this._lastname, this._insurance_number,
      this._company_phone, this._polis_link);

  String get company => _company;

  set company(String value) {
    _company = value;
  }

  get start_date => _start_date;

  get polis_link => _polis_link;

  set polis_link(value) {
    _polis_link = value;
  }

  get company_phone => _company_phone;

  set company_phone(value) {
    _company_phone = value;
  }

  get insurance_number => _insurance_number;

  set insurance_number(value) {
    _insurance_number = value;
  }

  get lastname => _lastname;

  set lastname(value) {
    _lastname = value;
  }

  get firstname => _firstname;

  set firstname(value) {
    _firstname = value;
  }

  get payment => _payment;

  set payment(value) {
    _payment = value;
  }

  get end_date => _end_date;

  set end_date(value) {
    _end_date = value;
  }

  set start_date(value) {
    _start_date = value;
  }


}