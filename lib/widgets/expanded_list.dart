import 'package:flutter/material.dart';
import '../config/colors_config.dart';
import '../config/text_styles_config.dart';
import '../model/question.dart';

/// list used to present questions
///
/// question is shown
/// when user presses on question -> answer to the question is shown
class QuestionSetWidget extends StatelessWidget {
  final QuestionsTopic set;

  QuestionSetWidget(this.set);

  @override
  Widget build(BuildContext context) {
    List<Widget> res = List();
    res.add(Row(
      children: [
        Expanded(
            child: Material(
                child: Container(
          child: Text(
            set.topic,
            style: TextStyles.h1,
          ),
          color: ColorsConfig.questionsBackground,
          padding: EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
        ))),
      ],
    ));

    for (int i = 0; i < set.questions.length; i++) {
      res.add(EntryItem(set.questions[i]));
    }
    return Column(
      children: res,
      crossAxisAlignment: CrossAxisAlignment.start,
    );
  }
}

class EntryItem extends StatelessWidget {
  const EntryItem(this.entry);

  final QuestionEntry entry;

  Widget _buildTile(QuestionEntry root) {
    return ExpansionTile(
      key: PageStorageKey<QuestionEntry>(root),
      title: Text(root.question,
          style: TextStyles.h3),
      children: [
        ListTile(
            title: Text(
          root.answer,
          style: TextStyles.text_small,
        ))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildTile(entry);
  }
}
