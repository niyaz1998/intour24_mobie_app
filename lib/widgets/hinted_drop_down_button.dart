import 'package:flutter/material.dart';
import '../config/text_styles_config.dart';

/// "Hinted" here is used for a small text in top-left side of the DropDownButton
/// this class for now is used only for dates in excursion screen
///
class HintedDropDownButton extends StatefulWidget {

  final List<String> dates;
  final _HintedDropDownState state = _HintedDropDownState();
  final String topLeftHint;
  final String preDate;


  HintedDropDownButton(this.dates, this.preDate, this.topLeftHint);

  @override
  State<StatefulWidget> createState() {
    return state;
  }

  String currentDate() {
    return state.currentDate();
  }
}

class _HintedDropDownState extends State<HintedDropDownButton> {

  String _selectedDate;


  @override
  void initState() {
    _selectedDate = widget.preDate;
    super.initState();
  }

  String currentDate() {
    return _selectedDate;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      Container(
          padding: EdgeInsets.only(top: 4.0),
          child: DropdownButton<String>(
        items: _getMenuDates(),
        onChanged: (String v) {
          setState(() {
            _selectedDate = v;
          });
        },
        value: _selectedDate,
      )) ,
      Text(
        widget.topLeftHint,
        style: TextStyles.text_xSmall,
      ),
    ],);
  }

  List<DropdownMenuItem<String>> _getMenuDates() {
    List<DropdownMenuItem<String>> res = List();
    for (String date in widget.dates) {
      res.add(DropdownMenuItem<String>(
        child: Text(date),
        value: date,
      ));
    }
    return res;
  }
}