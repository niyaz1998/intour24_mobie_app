import 'package:flutter/material.dart';
import '../config/icons_config.dart';

class IconTextWidget extends StatelessWidget {
  final IconData _iconData;
  final String _text;
  final double textSize;
  final Color color;

  IconTextWidget(this._iconData, this._text,
      {this.textSize = 16.0, this.color = Colors.black});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: EdgeInsets.only(top: 8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(_iconData, size: iconSize, color: color),
          Container(
              child: Text(
                _text,
                style: TextStyle(fontSize: textSize, color: color),
              ),
              padding: EdgeInsets.only(left: 8.0)),
        ],
      ),
    );
  }
}
