import 'package:flutter/material.dart';
import '../config/colors_config.dart';

/// button what contains:
/// image (obligatory)
/// gradient from top to center (black -> white) of the image
/// gradient from bottom to center (black -> white) of the image
/// it also allows to paste widgets at corners of the image
class GradientImageButton extends StatelessWidget {
  final double borderRadius, paddingSize, height_multiplier;
  final Widget topLeft, topRight, bottomLeft, bottomRight;
  final VoidCallback onPressed;
  final ImageProvider photo;

  /// height of the button will equal to height_multiplier * MediaQuery.of(context).size.width
  ///
  /// borderRadius radius of corners of the image
  /// padding size of the image and widgets inside it
  /// (button will pasted inside main container with padding size
  /// and widgets will be pasted inside containers in main container with specified padding size)
  ///
  /// topLeft, topRight, bottomLeft, bottomRight - optional widgets to place at the corners of the image
  ///
  /// Implementation:
  /// Stack of the widgets:
  /// image, gradients, widgets
  GradientImageButton(this.onPressed, this.photo,
      {this.height_multiplier = 0.36,
      this.borderRadius = 16.0,
      this.paddingSize = 16.0,
      this.topLeft = const Text(''),
      this.topRight = const Text(''),
      this.bottomLeft = const Text(''),
      this.bottomRight = const Text('')});

  @override
  Widget build(BuildContext context) {
    assert(height_multiplier > 0);
    double height = MediaQuery.of(context).size.width * height_multiplier;
    BorderRadius radius = BorderRadius.circular(borderRadius);

    return new RawMaterialButton(
      onPressed: onPressed,
      elevation: 8.0,
      padding: EdgeInsets.only(top: 8.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(16.0))),
      child: new Stack(
        children: <Widget>[
          Container(
            height: height,
            decoration: BoxDecoration(
              borderRadius: radius,
              image: DecorationImage(
                fit: BoxFit.fill,
                image: photo,
              ),
            ),
          ),
          Container(
            height: height,
            decoration: BoxDecoration(
                borderRadius: radius,
                gradient: LinearGradient(
                    begin: FractionalOffset.topCenter,
                    end: FractionalOffset.center,
                    colors: ColorsConfig.gradientColors,
                    stops: [0.0, 1.0])),
          ),
          Container(
            height: height,
            decoration: BoxDecoration(
                borderRadius: radius,
                gradient: LinearGradient(
                    begin: FractionalOffset.bottomCenter,
                    end: FractionalOffset.center,
                    colors: ColorsConfig.gradientColors,
                    stops: [0.0, 1.0])),
          ),

          // optional childs
          Positioned(
            top: paddingSize,
            left: paddingSize,
            right: paddingSize,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [Expanded(child: topLeft), topRight]
            ),
          ),
          Positioned(
            bottom: paddingSize,
            left: paddingSize,
            right: paddingSize,
            child: Row(
                children: [Expanded(child: bottomLeft), bottomRight]
            ),
          ),
        ],
      ),
    );
  }
}
