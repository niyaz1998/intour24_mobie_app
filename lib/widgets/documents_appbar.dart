import 'package:flutter/material.dart';
import 'icon_text_widget.dart';
import '../config/text_styles_config.dart';

/// app bar in the documents fragment
/// contains information about current loaction
///
/// how "current location" will be found - a question
class DocumentAppBar extends StatelessWidget {
  final String country, city, weather, exchangeRate;

  const DocumentAppBar(
      this.country, this.city, this.weather, this.exchangeRate,
      {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        // TODO: 32.0 padding is here so toolbar will not overlap status bar
        // this is needed to be done another way
        padding: EdgeInsets.fromLTRB(16.0, 32.0, 8.0, 8.0),
        child: Column(
          children: [
            Text(
              country + ' ' + city,
              style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            ),
            SizedBox(
              height: 16.0,
            ),
            Row(children: [
              Container(
                  padding: EdgeInsets.only(right: 24.0),
                  child: Column(
                    children: [
                      Text('сегодня', style: TextStyles.label_xSmall.copyWith(color: Colors.white)),
                      IconTextWidget(
                        Icons.wb_sunny,
                        weather,
                        color: Colors.white,
                        textSize: 18.0,
                      ) // style_h2
                    ],
                    crossAxisAlignment: CrossAxisAlignment.start,
                  )),
              Column(
                children: [
                  Text('курс валюты', style: TextStyles.label_xSmall.copyWith(color: Colors.white)),
                  Text(exchangeRate, style: TextStyles.h2.copyWith(color: Colors.white))
                ],
                crossAxisAlignment: CrossAxisAlignment.start,
              )
            ])
          ],
          crossAxisAlignment: CrossAxisAlignment.start,
        ));
  }
}
