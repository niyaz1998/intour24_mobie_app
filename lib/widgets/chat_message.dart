import 'package:flutter/material.dart';
import '../model/chat_message.dart';
import '../config/colors_config.dart';
import '../config/text_styles_config.dart';

class ChatMessageWidget extends StatelessWidget {
  final ChatMessage message;
  final double maxTextWidth = 250.0;

  const ChatMessageWidget({Key key, this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (message.sender == MessageSender.agent) {
      return _getAgentMessageWidget();
    } else {
      return _getUserMessageWidget();
    }
  }

  Widget _getAgentMessageWidget() {
    return Row(
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
          constraints: BoxConstraints(maxWidth: maxTextWidth),
          child: Text(
            message.text,
            style: TextStyles.label_large,
          ),
          decoration: BoxDecoration(
              color: ColorsConfig.silverSand,
              borderRadius: BorderRadius.all(Radius.circular(16.0))),
        ),
        Text(
          message.time.toLocal().hour.toString() +
              ':' +
              message.time.toLocal().minute.toString(),
          style: TextStyle(fontSize: 8.0),
        )
      ],
    );
  }

  Widget _getUserMessageWidget() {
    IconData iconStatus = Icons.check;
    if (message.status == MessageStatus.sending) {
      iconStatus = Icons.access_time;
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Column(
          children: <Widget>[
            Text(
              message.time.toLocal().hour.toString() +
                  ':' +
                  message.time.toLocal().minute.toString(),
              style: TextStyle(fontSize: 8.0),
            ),
            Icon(iconStatus, size: 12.0,)
          ],
        ),
        Container(
          padding: EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
          constraints: BoxConstraints(maxWidth: maxTextWidth),
          child: Text(
            message.text,
            style: TextStyle(fontSize: 16.0, color: Colors.white),
          ),
          decoration: BoxDecoration(
              color: ColorsConfig.dodgerBlue,
              borderRadius: BorderRadius.all(Radius.circular(16.0))),
        ),
      ],
    );
  }
}
