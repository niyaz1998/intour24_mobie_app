import 'package:flutter/material.dart';
import '../config/text_styles_config.dart';

/// "Hinted" here is used for a small text in top-left side of the DropDownButton
/// this class for now is used only for input amount of excursions to purchase
///
///
class HintedSumDropDownButton extends StatefulWidget {
  final double cost, width;
  final _HintedSumDropDownState state = _HintedSumDropDownState();
  final String topLeftHint, currency;

  HintedSumDropDownButton(
      this.cost, this.topLeftHint, this.currency, this.width);

  @override
  State<StatefulWidget> createState() {
    return state;
  }

  double currentCost() {
    return state.currentCost();
  }
}

class _HintedSumDropDownState extends State<HintedSumDropDownButton> {
  int amount;

  @override
  void initState() {
    amount = 0;
    super.initState();
  }

  double currentCost() {
    return amount * widget.cost;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      child: Stack(
        children: <Widget>[
          Row(children: [
            Expanded(
                child: Container(
                    padding: EdgeInsets.only(top: 4.0),
                    child: DropdownButton<int>(
                      items: _getInts(),
                      onChanged: (int v) {
                        setState(() {
                          amount = v;
                        });
                      },
                      value: amount,
                    )))
          ]),
          Positioned(
            child: Text(
              _getCost(),
              style: TextStyles.text_large,
            ),
            bottom: 16.0,
            right: 0.0,
          ),
          Text(
            widget.topLeftHint,
            style: TextStyles.text_xSmall,
          ),
        ],
      ),
    );
  }

  List<DropdownMenuItem<int>> _getInts() {
    List<DropdownMenuItem<int>> res = List(100);
    for (int i = 0; i < 100; i++) {
      res[i] = DropdownMenuItem<int>(
        child: Text(
          i.toString(),
          style: TextStyles.text_large,
        ),
        value: i,
      );
    }
    return res;
  }

  String _getCost() {
    return (amount * widget.cost).toString() + ' ' + widget.currency;
  }
}
