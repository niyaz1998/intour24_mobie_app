import 'package:flutter/material.dart';
import '../model/excursion.dart';
import 'gradient_image_button.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../config/icons_config.dart';
import '../screens/excursion_screen.dart';
import '../config/text_styles_config.dart';

/// An image with rounded corners
/// image is provided by server
///
/// in the corners of the image some information placed:
/// top left: start time
/// top right: category and servces icons
/// bottom left: short title
/// bottom right: cost
class ExcursionWidget extends StatelessWidget {
  final Excursion excursion;
  final String date;

  ExcursionWidget(this.excursion, this.date);

  @override
  Widget build(BuildContext context) {
    return GradientImageButton(() {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ExcursionScreen(excursion, date),
          ));
    }, CachedNetworkImageProvider(excursion.photoLink),
        height_multiplier: 4 / 7,
        topLeft: Text(
          excursion.startTime,
          style: TextStyles.label_small.copyWith(color: Colors.white),
        ),
        topRight: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Icon(
              ExcursionsCategoriesIcons.getIcon(excursion.type),
              size: iconSize,
              color: Colors.white,
            ),
            Wrap(
              alignment: WrapAlignment.end,
              children: _getServicesIcons(),
            )
          ],
        ),
        bottomLeft: Text(
          excursion.title,
          style: TextStyles.h2.copyWith(color: Colors.white),
        ),
        bottomRight: Text(
          excursion.cost,
          style: TextStyles.text_large.copyWith(color: Colors.white),
        ));
  }

  List<Widget> _getServicesIcons() {
    List<String> split = excursion.getCodes();
    List<Widget> res = List();
    for (int i = 0; i < split.length; i++) {
      res.add(Icon(
        ExcursionServicesIcons.getIcon(split[i]),
        size: iconSize,
        color: Colors.white,
      ));
      //res.add(SizedBox(width: 4.0, height: 4.0,));
    }
    //res.removeLast();
    return res;
  }
}
