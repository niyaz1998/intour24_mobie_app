import 'package:flutter/material.dart';
import 'screens/phone_login_screen.dart';
import 'screens/chat_screen.dart';
import 'config/colors_config.dart';
import 'screens/main_screen.dart';
import 'screens/passports_screen.dart';
import 'screens/questions_screen.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'screens/purchased_excursions_screen.dart';

void main() {
  CacheManager.inBetweenCleans =
      new Duration(days: 30); // cache manager will store files for 30 days
  // \cache manager is used by cached_network_image library to store excursions photos
  runApp(Intour24());
}

class Intour24 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: new ThemeData(
        primaryColor: ColorsConfig.flamingo,
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      title: 'Intour24',
      routes: {
        '/phone_login': (BuildContext context) => PhoneLogin(),
        '/': (BuildContext context) => HomePage(),
        '/passports': (BuildContext context) => PassportsScreen(),
        '/questions': (BuildContext context) => QuestionsScreen(),
        '/agent_chat': (BuildContext context) => ChatScreen(),
        '/purchased_excursions': (BuildContext context) => PurchasedExcursionsScreen(),
      },
    );
  }
}
