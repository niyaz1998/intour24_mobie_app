import '../model/excursion.dart';
import '../helpers/DateHelper.dart';


/// this class provides helping functions in managing excursions
/// such as:
///   1) list of all dates when excursions are present
///   2) list of excursions available in current date
///   3) allows to move between dates
class ExcursionManager {
  
  /// list of all excursions
  final List<Excursion> excursions;

  /// sorted list of dates
  /// in this dates at least one excursion is available
  List<String> excursionsDates;

  /// index of date in [excursionsDates] list
  int currentDateIndex;

  /// maps date to the list of excursions in that date
  Map<String, List<Excursion>> excursionsInDate;

  ExcursionManager(this.excursions) {
    // this line is needed because I don't want to add if's into functions
    assert(excursions.length != 0);
    _setExcursionDates();
    _setExcursionsInDates();
    _setCurrentDate();
  }

  /// tells if there is some day after current with excursion(s)
  bool hasNextDate() {
    return currentDateIndex < excursionsDates.length - 1;
  }

  /// tells if there is some day before current with excursion(s)
  bool hasPreviousDate() {
    return currentDateIndex > 0;
  }


  /// moves to the next date and returns it's string representation
  ///
  /// returns null if there is no next day
  String moveToNextDate() {
    if (currentDateIndex < excursionsDates.length - 1) {
      currentDateIndex++;
      return excursionsDates[currentDateIndex];
    }
    return null;
  }

  /// moves to the previous date and returns it's string representation
  ///
  /// returns null if there is no previous day
  String moveToPreviousDate() {
    if (currentDateIndex > 0) {
      currentDateIndex--;
      return excursionsDates[currentDateIndex];
    }
    return null;
  }

  /// returns list of excursions in current day
  List<Excursion> getCurrentExcursions() {
    return excursionsInDate[excursionsDates[currentDateIndex]];
  }

  String getCurrentDate() {
    return excursionsDates[currentDateIndex];
  }

  /// creates [excursionsDates] list
  void _setExcursionDates() {
    Set<String> res = Set();
    for (int i = 0; i < excursions.length; i++) {
      for (int y = 0; y < excursions[i].excursionDates.length; y++) {
        res.add(excursions[i].excursionDates[y]);
      }
    }
    excursionsDates = res.toList(growable: false);
    excursionsDates.sort((String s1, String s2) {
      //dd.mm.yyyy,
      DateTime d1 = DateHelper.convert(s1);
      DateTime d2 = DateHelper.convert(s2);
      return d1.compareTo(d2);
    });
  }

  /// creates [excursionsInDate] map
  void _setExcursionsInDates() {
    excursionsInDate = Map();
    for (int i = 0; i < excursionsDates.length; i++) {
      excursionsInDate[excursionsDates[i]] = List();
    }

    for (int i = 0; i < excursions.length; i++) {
      for (int y = 0; y < excursions[i].excursionDates.length; y++) {
        excursionsInDate[excursions[i].excursionDates[y]].add(excursions[i]);
      }
    }
  }

  /// sets [currentDateIndex] to the index of date in [excursionsDates] list
  /// so that date under this index in the list will be bigger or equal to the
  /// today
  ///
  /// if all dates in [excursionsDates] list are less than today,
  /// [currentDateIndex] will point to the last date in the list
  void _setCurrentDate() {
    DateTime now = DateTime.now();
    for (int i = 0; i < excursionsDates.length; i++) {
      if (DateHelper.convert(excursionsDates[i]).compareTo(now) >= 0) {
        currentDateIndex = i;
        return;
      }
    }
    currentDateIndex = excursionsDates.length - 1;
  }
}
