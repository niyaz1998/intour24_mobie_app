import '../model/tourist.dart';
import '../model/flight.dart';
import '../model/insurance.dart';
import '../model/hotel.dart';
import '../model/ground_transfer.dart';

/// interface which is needed to the application to display information
abstract class Repository {

  List<Tourist>getTourists();

  List<Flight> getFlights();

  List<GroundTransfer> getTransfers();

  List<Hotel> getHotels();

  List<Insurance> getInsurances();
}