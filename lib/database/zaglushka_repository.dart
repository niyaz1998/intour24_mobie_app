import 'package:intour24_mobile_app/model/flight.dart';
import 'package:intour24_mobile_app/model/ground_transfer.dart';
import 'package:intour24_mobile_app/model/hotel.dart';
import 'package:intour24_mobile_app/model/insurance.dart';

import '../model/tourist.dart';
import 'repository.dart';

class MyRepository implements Repository {
  @override
  List<Tourist> getTourists() {
    List<Tourist> res = new List();
    res.add(new Tourist('Daniil', 'Botnarenkuuuuuuuuuuuuuuuu', 'M', '-11.12.1111', '11111111111', 'Ottuda'));
    res.add(new Tourist('Andrey', 'Pavkekko', 'M', '11.12.1111', '11111111111', 'Ottuda'));
    res.add(new Tourist('Niyaz', 'Giniatullin', 'M', '11.12.1111', '11111111111', 'Ottuda'));
    return res;
  }

  @override
  List<Flight> getFlights() {
    List<Flight> res = new List();
    res.add(Flight('niyaz', 'ginatullin', 'some tick num', 'som tick class',
        'some luggage', 'destination', '000000000', 'airoport', 'aaaaa', 'aaaaaa'));
    res.add(Flight('aigiz', 'ginatullin', 'some tick num', 'som tick class',
        'some luggage', 'destination', '000000000', 'airoport', 'aaaaa', 'aaaaaa'));
    return res;
  }

  @override
  List<Hotel> getHotels() {
    List<Hotel> res = new List();
    res.add(Hotel('title 1', 'address 1', 'room code', 'cater code', 'vouchers ling'));
    res.add(Hotel('title 2', 'address 2', 'room code', 'cater code', 'vouchers ling'));
    return res;
  }

  @override
  List<Insurance> getInsurances() {
    List<Insurance> res = new List();
    res.add(Insurance('comany 1', 'st', 'ed', 'pay', 'name', 'lastname', 'number', 'phone', 'some link'));
    res.add(Insurance('comany 2', 'st', 'ed', 'pay', 'name', 'lastname', 'number', 'phone', 'some link'));
    return res;
  }

  @override
  List<GroundTransfer> getTransfers() {
    List<GroundTransfer> res = new List();
    res.add(GroundTransfer('type 1', 'datetime', 'place 1', 'destination'));
    res.add(GroundTransfer('type 2', 'datetime', 'place 2', 'destination'));
    return res;
  }



}