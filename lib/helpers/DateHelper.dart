


class DateHelper {
  // 'dd.mm.yyyy' is default format of date received from server and used in this applicaton

  /// this class helps transform Strings received from server into DateTime objects

  static DateTime convert(String date) {
    List<String> parsed = date.split('.');
    return DateTime(int.parse(parsed[2]),
        int.parse(parsed[1]),
        int.parse(parsed[0]),
        0,0,0,0,0);
  }
}