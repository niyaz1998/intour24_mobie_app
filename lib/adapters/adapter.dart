import 'package:flutter/material.dart';


abstract class WidgetListAdapter {

  List<Widget> getChildren();
}