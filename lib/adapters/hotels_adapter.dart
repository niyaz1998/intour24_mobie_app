import 'package:flutter/material.dart';
import 'adapter.dart';
import 'package:intour24_mobile_app/widgets/icon_text_widget.dart';
import '../model/hotel.dart';
import '../widgets/gradient_image_button.dart';

class HotelsAdapter implements WidgetListAdapter {
  List<Hotel> hotels;

  HotelsAdapter(this.hotels);

  @override
  List<Widget> getChildren() {
    List<Widget> res = new List();
    for (int i = 0; i < hotels.length; i++) {
      res.add(_getFlightWidget(i));
    }
    return res;
  }

  Widget _getFlightWidget(int i) {
    Hotel hotel = hotels[i];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        IconTextWidget(Icons.place,
            hotel.title + '\n' + hotel.address),
        IconTextWidget(
            Icons.info,
            hotel.room_code + '\n' + hotel.catering_code),
        GradientImageButton(() {}, AssetImage('assets/images/hotel_button.jpg'),)
      ],
    );
  }
}
