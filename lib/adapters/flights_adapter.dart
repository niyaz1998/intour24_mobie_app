import 'package:flutter/material.dart';
import 'adapter.dart';
import 'package:intour24_mobile_app/widgets/icon_text_widget.dart';
import '../model/flight.dart';
import '../config/colors_config.dart';
import '../widgets/gradient_image_button.dart';

class FlightsAdapter implements WidgetListAdapter {
  List<Flight> flights;

  FlightsAdapter(this.flights);

  @override
  List<Widget> getChildren() {
    List<Widget> res = new List();
    for (int i = 0; i < flights.length; i++) {
      res.add(_getFlightWidget(i));
    }
    return res;
  }

  Widget _getFlightWidget(int i) {
    Flight flight = flights[i];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        IconTextWidget(Icons.airplanemode_active,
            flight.flight_datetime + '\n' + flight.destination),
        IconTextWidget(
            Icons.description,
            flight.firstname +
                ' ' +
                flight.lastname +
                '\n' +
                flight.ticket_number +
                '\n' +
                flight.ticket_class),
        _getFlightPdfButton(),
        _flightRegistrationButton(),
      ],
    );
  }

  Widget _getFlightPdfButton() {
    return GradientImageButton(
      () {}, AssetImage('assets/images/plane_button.jpg'),
    );
  }

  Widget _flightRegistrationButton() {
    return Container(
        padding: EdgeInsets.only(top: 8.0),
        child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0)),
            onPressed: () {},
            color: ColorsConfig.flamingo,
            child: Container(
              padding: EdgeInsets.all(16.0),
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Регистрация на рейс',
                    style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  )),
            )));
  }
}
