import 'package:flutter/material.dart';
import '../model/tourist.dart';
import 'package:intour24_mobile_app/widgets/icon_text_widget.dart';
import 'adapter.dart';


class TouristsAdapter implements WidgetListAdapter {

  List<Tourist> tourists;

  TouristsAdapter(this.tourists);

  @override
  List<Widget> getChildren() {
    List<Widget> res = new List();
    for (int i = 0; i < tourists.length; i++) {
      res.add(_getTouristsWidget(i));
    }
    return res;
  }

  Widget _getTouristsWidget(int i) {
    Tourist tourist = tourists[i];
    return IconTextWidget(Icons.accessibility,
        tourist.firstname + ' ' + tourist.lastname + '\n' +
        tourist.birthdate + ' ' + tourist.gender + '\n' +
        tourist.passport);
  }

}