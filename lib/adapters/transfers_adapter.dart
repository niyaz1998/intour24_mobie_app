import 'package:flutter/src/widgets/framework.dart';
import '../model/ground_transfer.dart';
import 'package:flutter/material.dart';
import '../widgets/icon_text_widget.dart';

import 'adapter.dart';

class TransferAdapter extends WidgetListAdapter {

  List<GroundTransfer> transfers;

  TransferAdapter(this.transfers);

  @override
  List<Widget> getChildren() {
    List<Widget> res = new List();
    for (int i = 0; i < transfers.length; i++) {
      res.add(_getTouristsWidget(i));
    }
    return res;
  }

  Widget _getTouristsWidget(int i) {
    GroundTransfer transfer = transfers[i];
    return IconTextWidget(Icons.directions_car,
        transfer.datetime + ' ' + transfer.type
    + '\n' + transfer.departure_place + ' ' + transfer.destination);
  }

}