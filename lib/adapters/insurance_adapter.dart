import 'package:flutter/material.dart';
import 'adapter.dart';
import 'package:intour24_mobile_app/widgets/icon_text_widget.dart';
import '../model/insurance.dart';
import '../config/colors_config.dart';
import '../widgets/gradient_image_button.dart';

class InsuranceAdapter implements WidgetListAdapter {
  List<Insurance> insurances;

  InsuranceAdapter(this.insurances);

  @override
  List<Widget> getChildren() {
    List<Widget> res = new List();
    for (int i = 0; i < insurances.length; i++) {
      res.add(_getFlightWidget(i));
    }
    return res;
  }

  Widget _getFlightWidget(int i) {
    Insurance insurance = insurances[i];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        IconTextWidget(
            Icons.account_balance,
            insurance.company +
                '\n' +
                insurance.start_date +
                ' ' +
                insurance.end_date),
        IconTextWidget(
            Icons.description,
            insurance.firstname +
                ' ' +
                insurance.lastname +
                '\n' +
                insurance.insurance_number +
                '\n' +
                insurance.payment),
        _getInsurancePdfButton(),
        _contactForInsuranceAccident(),
      ],
    );
  }

  Widget _getInsurancePdfButton() {
    return GradientImageButton(() {}, AssetImage('assets/images/insurance_button.jpg'),);
  }

  Widget _contactForInsuranceAccident() {
    return Container(
        padding: EdgeInsets.only(top: 8.0),
        child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0)),
            onPressed: () {},
            color: ColorsConfig.flamingo,
            child: Container(
              padding: EdgeInsets.all(16.0),
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Связаться по страховому случаю',
                    style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  )),
            )));
  }
}
