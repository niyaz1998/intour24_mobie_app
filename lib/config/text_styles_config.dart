import 'package:flutter/material.dart';

class TextStyles {
  static TextStyle h1 = TextStyle(fontSize: 20.0, fontWeight: FontWeight.w500);
  static TextStyle h2 = TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500);
  static TextStyle h3 = TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500);

  static TextStyle text_large = TextStyle(fontSize: 16.0, fontWeight: FontWeight.w400);
  static TextStyle text_small = TextStyle(fontSize: 14.0, fontWeight: FontWeight.w400);
  static TextStyle text_xSmall = TextStyle(fontSize: 12.0, fontWeight: FontWeight.w400);

  // requires capitalization
  static TextStyle label_large = TextStyle(
    fontSize: 18.0, fontWeight: FontWeight.w500
  );
  static TextStyle label_small = TextStyle(fontSize: 16.0, letterSpacing: -0.4, fontWeight: FontWeight.w500);
  // requires capitalization
  static TextStyle label_xSmall =
      TextStyle(fontSize: 14.0, letterSpacing: 0.5, fontWeight: FontWeight.w500);
}
