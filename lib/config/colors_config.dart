import 'package:flutter/material.dart';

/// contains main colors of the application
class ColorsConfig {

  static Color flamingo = Color(0xFFFF5621);
  static Color kilimanjaro = Color(0xFF270900);
  static Color dodgerBlue = Color(0xFF2998FF);
  static Color silverSand = Color(0xFFC3C3C3);
  static Color ufoGreen = Color(0xFF1ee577);
  static Color harvardGreen = Color(0xFFd0021b);
  static Color questionsBackground =  Color(0xFFF5F5F5);

  static Color gradientStart = Colors.black.withOpacity(0.4);
  static Color gradientMiddle = Colors.grey.withOpacity(0.0);

  static List<Color> gradientColors = [gradientStart, gradientMiddle];
}