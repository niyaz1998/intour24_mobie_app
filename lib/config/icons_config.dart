import 'package:flutter/material.dart';
import 'my_flutter_app_icons.dart';

final iconSize = 24.0; // default icon size for this application

/// icons and titles for types of the question sections
/// used in questions screen
class QuestionsSectionIcons {

  static IconData getIcon(String code) {
    switch(code.toUpperCase()) {
      case 'CASE':
        return MyFlutterApp.suitcase;
      case 'GLOB':
        return Icons.public;
      case 'PHON':
        return Icons.phone;
      case 'MASK':
        return MyFlutterApp.theatre;
      default:
        return Icons.error_outline;
    }
  }

  static String getString(String code) {
    switch(code.toUpperCase()) {
      case 'CASE':
        return 'сборы в поездку';
      case 'GLOB':
        return 'место отдыха';
      case 'PHON':
        return 'страховые случаи';
      case 'MASK':
        return 'экскурсии и развлечения';
      default:
        return 'произошла ошибка';
    }
  }
}

/// icons and titles for types of the excursions
/// used in excursion screen and excursion fragment
class ExcursionsCategoriesIcons {

  static IconData getIcon(String code) {
    switch(code.toUpperCase()) {
      case 'WTCH':
        return MyFlutterApp.binoculars;
      case 'HSTR':
        return MyFlutterApp.history;
      case 'FOOD':
        return Icons.room_service;
      case 'CULT':
        return Icons.account_balance;
      case 'WAVE':
        return MyFlutterApp.waves;
      case 'SURF':
        return MyFlutterApp.soccer_ball;
      case 'CONF':
        return Icons.extension;
      default:
        return Icons.error_outline;
    }
  }

  static String getString(String code) {
    switch(code.toUpperCase()) {
      case 'WTCH':
        return 'обзорная';
      case 'HSTR':
        return 'историческая';
      case 'FOOD':
        return 'гастрономическая';
      case 'CULT':
        return 'культурная';
      case 'WAVE':
        return 'морская';
      case 'SURF':
        return 'спортивная';
      case 'CONF':
        return 'развлекательная';
      default:
        return 'ошибка';
    }
  }
}

/// icons and titles for types of the services provided by excursion
/// used in excursion screen and excursion fragment
class ExcursionServicesIcons {

  static IconData getIcon(String code) {
    switch(code.toUpperCase()) {
      case 'HUMN':
        return Icons.accessibility;
      case 'LINE':
        return Icons.timeline;
      case 'BUS':
        return Icons.directions_bus;
      case 'BOAT':
        return Icons.directions_boat;
      case 'DIVE':
        return MyFlutterApp.diving;
      case 'BIKE':
        return Icons.directions_bike;
      case 'TICT':
        return MyFlutterApp.ticket;
      case 'ICEC':
        return MyFlutterApp.fort_awesome;
      case 'FORK':
        return Icons.restaurant;
      case 'MASK':
        return MyFlutterApp.theatre;
      case 'BEDH':
        return Icons.hotel;
      default:
        return Icons.error_outline;
    }
  }

  static String getString(String code) {
    switch(code.toUpperCase()) {
      case 'HUMN':
        return 'экскурсовод';
      case 'LINE':
        return 'пешеходный маршрут';
      case 'BUS':
        return 'туристический автобус';
      case 'BOAT':
        return 'морская прогулка';
      case 'DIVE':
        return 'спортивная экипировка';
      case 'BIKE':
        return 'спортивный транспорт';
      case 'TICT':
        return 'билеты в музеи';
      case 'ICEC':
        return 'дегустации';
      case 'FORK':
        return 'питание';
      case 'MASK':
        return 'театрализованное шоу';
      case 'BEDH':
        return 'ночевка в отеле';
      default:
        return 'ошибка';
    }
  }

}