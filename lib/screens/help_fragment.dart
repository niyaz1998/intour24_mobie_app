import 'package:flutter/material.dart';
import '../widgets/expanded_list.dart';
import '../config/colors_config.dart';
import '../config/icons_config.dart';
import '../config/text_styles_config.dart';
import '../model/question.dart';

/// contains button to call your agent
/// button to navigate to the chat with tour agent
/// list of frequently asked question (FAQ)
/// button to navigate to screen with all questions
class HelpScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _getAppBar(),
      backgroundColor: ColorsConfig.questionsBackground,
      body: Container(
          child: ListView(
        children: <Widget>[
          Container(
            child: Text(
              'Чат с турагентом',
              style: TextStyles.h1,
            ),
            padding: EdgeInsets.fromLTRB(16.0, 24.0, 0.0, 0.0),
          ),
          _getStartChatButton(context),
          _getFAQ(),
          _getAllQuestionsButton(context)
        ],
      )),
    );
  }

  Widget _getStartChatButton(BuildContext context) {
    return RawMaterialButton(
      onPressed: () {
        Navigator.pushNamed(context, '/agent_chat');
      },
      fillColor: Colors.white,
      child: Row(
        children: <Widget>[
          Container(
              padding: EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
              child: Icon(
                Icons.account_circle,
                size: 36.0,
              )),
          Expanded(
            child: Text(
              'Начать чат',
              style: TextStyles.h1,
            ),
          ),
          Container(
            child: Icon(
              Icons.chat,
              size: iconSize,
            ),
            padding: EdgeInsets.only(right: 16.0),
          ),
        ],
      ),
    );
  }

  Widget _getFAQ() {
    return Container(
      child: QuestionSetWidget(data),
      color: Colors.white,
    );
  }

  Widget _getAllQuestionsButton(BuildContext context) {
    return RawMaterialButton(
      onPressed: () {
        Navigator.pushNamed(context, '/questions');
      },
      fillColor: ColorsConfig.dodgerBlue,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              child: Text(
                'Посмотреть все вопросы',
                style: TextStyles.text_large.copyWith(color: Colors.white),
              ),
              padding: EdgeInsets.all(16.0),
            ),
          ),
          Container(
            child: Icon(
              Icons.keyboard_arrow_right,
              color: Colors.white,
            ),
            padding: EdgeInsets.only(right: 16.0),
          )
        ],
      ),
    );
  }

  /// contains button to call your agent
  AppBar _getAppBar() {
    return AppBar(
      title: Text('Помощь'),
      actions: [IconButton(icon: Icon(Icons.call), onPressed: () {})],
    );
  }
}
