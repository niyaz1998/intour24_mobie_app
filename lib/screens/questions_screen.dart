import 'package:flutter/material.dart';
import '../widgets/expanded_list.dart';
import '../model/question.dart';
import '../config/text_styles_config.dart';

/// contains list of all questions
/// and allows to search question or topic by app bar
class QuestionsScreen extends StatefulWidget {
  @override
  _QuestionsScreenState createState() => _QuestionsScreenState();
}

class _QuestionsScreenState extends State<QuestionsScreen> {
  final TextEditingController _filter = TextEditingController();
  String _searchText = "";
  List<QuestionsTopic> data;
  Icon _searchIcon = Icon(Icons.search);
  Widget _appBarTitle = Text('Вопросы');

  _QuestionsScreenState() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
    });
  }

  @override
  void initState() {
    this._getData();
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildBar(context),
      body: Container(
        child: _buildList(),
      ),
      resizeToAvoidBottomPadding: false,
    );
  }

  Widget _buildBar(BuildContext context) {
    return new AppBar(centerTitle: true, title: _appBarTitle, actions: [
      IconButton(
        icon: _searchIcon,
        onPressed: _searchPressed,
      ),
    ]);
  }

  Widget _buildList() {
    // if search text is big enough -> apply filter
    // else -> display all data
    if (_searchText.length > 2) {
      List<Widget> result = List();
      for (int i = 0; i < data.length; i++) {
        // if topic contains search text -> display all topic
        // else -> display only questions that contains search text
        if (data[i].topic.toLowerCase().contains(_searchText.toLowerCase())) {
          result.add(QuestionSetWidget(data[i]));
        } else {
          List<QuestionEntry> tempList = List();
          for (int y = 0; y < data[i].questions.length; y++) {
            if (data[i]
                    .questions[y]
                    .answer
                    .toLowerCase()
                    .contains(_searchText.toLowerCase()) ||
                data[i]
                    .questions[y]
                    .question
                    .toLowerCase()
                    .contains(_searchText.toLowerCase())) {
              tempList.add(data[i].questions[y]);
            }
          }
          // if topic doesn't contain any question that contains search text ->
          // don't add it to resulting list
          if (tempList.isNotEmpty) {
            result.add(
                QuestionSetWidget(QuestionsTopic(data[i].topic, tempList)));
          }
        }
      }
      return ListView(
        children: result,
      );
    } else {
      return ListView.builder(
        itemCount: data == null ? 0 : data.length,
        itemBuilder: (BuildContext context, int index) {
          return QuestionSetWidget(data[index]);
        },
      );
    }
  }

  void _searchPressed() {
    setState(() {
      if (this._searchIcon.icon == Icons.search) {
        this._searchIcon = Icon(
          Icons.close,
          color: Colors.white,
        );
        this._appBarTitle = TextField(
          cursorColor: Colors.white,
          controller: _filter,
          style: TextStyles.text_large.copyWith(color: Colors.white),
          decoration: InputDecoration(
              enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
              prefixIcon: Icon(
                Icons.search,
                color: Colors.white,
              ),
              hintText: 'Поиск...',
              hintStyle: TextStyle(color: Colors.white)),
        );
      } else {
        this._searchIcon = Icon(Icons.search);
        this._appBarTitle = Text('Вопросы');
        _filter.clear();
      }
    });
  }

  void _getData() {
    setState(() {
      data = mainData;
    });
  }
}
