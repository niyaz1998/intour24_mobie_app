import 'package:flutter/material.dart';
import '../adapters/tourists_adapter.dart';
import '../database/zaglushka_repository.dart';
import '../config/colors_config.dart';
import '../adapters/flights_adapter.dart';
import '../adapters/transfers_adapter.dart';
import '../adapters/hotels_adapter.dart';
import '../adapters/insurance_adapter.dart';
import '../widgets/documents_appbar.dart';
import '../config/icons_config.dart';
import '../config/text_styles_config.dart';

/// contains main information about the list
/// list of tourists, flights, ground transfers, hotels, insurances

class DocumentsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    const appBar = DocumentAppBar(
      'Country',
      'city',
      'weather',
      'rate',
    );
    return Scaffold(
        body: new CustomScrollView(slivers: <Widget>[
      const SliverAppBar(
        flexibleSpace: FlexibleSpaceBar(
          background: appBar,
        ),
        floating: true,
        expandedHeight: 122.0,
      ),
      new SliverList(delegate: new SliverChildListDelegate(_getMainInfoList(context)))
    ],
        ));
  }

  /// forms a list to be displayed on main screen
  ///
  /// as long as I didn't find a way to set some padding between elements in the list
  /// I just add sized boxes, it will get work done, don't worry
  List<Widget> _getMainInfoList(BuildContext context) {
    List<Widget> res = new List();

    void _padding() {
      res.add(SizedBox(
        height: 8.0,
      ));
    }

    _padding();
    res.add(_getSubTitle('Отдыхающие'));
    res = _addWidgets(
        res, TouristsAdapter(MyRepository().getTourists()).getChildren());
    _padding();
    res.add(_getPassportsButton(context));
    _padding();
    res.add(_getSubTitle('Авиаперелёт'));
    res = _addWidgets(
        res, FlightsAdapter(MyRepository().getFlights()).getChildren());
    _padding();
    res.add(_getSubTitle('Трансфер'));
    res = _addWidgets(
        res, TransferAdapter(MyRepository().getTransfers()).getChildren());
    _padding();
    res.add(_getSubTitle('Отель'));
    res = _addWidgets(
        res, HotelsAdapter(MyRepository().getHotels()).getChildren());
    _padding();
    res.add(_getSubTitle('Страховка'));
    res = _addWidgets(
        res, InsuranceAdapter(MyRepository().getInsurances()).getChildren());

    for (int i = 0; i < res.length; i++) {
      res[i] = Container(
        padding: EdgeInsets.only(left: 8.0, right: 8.0),
        child: res[i],
      );
    }
    res.add(SizedBox(height: 8.0));
    return res;
  }

  // TODO: code below adds padding to all children in the list, redo this if possible
  List _addWidgets(List<Widget> res, List<Widget> other) {
    for (int i = 0; i < other.length; i++) {
      res.add(SizedBox(
        height: 8.0,
      ));
      res.add(other[i]);
    }
    return res;
  }

  /// returns a blue button with text on rights side and icon on left
  /// button to open passports screen
  Widget _getPassportsButton(BuildContext context) {
    return Container(
        height: 56.0,
        child: RaisedButton(
          color: ColorsConfig.dodgerBlue,
          textColor: Colors.white,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text('Фото паспортов',
                  style: TextStyles.label_small,),
              Expanded(
                child: Align(
                  child: Icon(
                    Icons.camera_alt,
                    color: Colors.white,
                    size: iconSize,
                  ),
                  alignment: Alignment.centerRight,
                ),
              )
            ],
          ),
          onPressed: () {
            Navigator.pushNamed(context, '/passports');
          },
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        ));
  }

  Widget _getSubTitle(String s) {
    return Container(
        child: Text(
      s,
      style: TextStyles.h2,
    ));
  }
}
