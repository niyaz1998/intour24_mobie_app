import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import '../config/colors_config.dart';
import 'package:image_picker/image_picker.dart';
import 'photo_screen.dart';
import '../config/icons_config.dart';
import '../config/text_styles_config.dart';

/// this classes responsible for passports photos
/// it shows images made by user as a GridView
/// (last element of GridView is a button for making new photos)
///
/// when photo is made it will be stored in application's documents path
/// so only this application will access photos
///
/// names of the files will be formed as:
/// '<application documents directory>/Pictures/<file name>.jpg'
///
/// <application documents directory> is provided by path_provider plugin

class PassportsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PassportsState();
}

class _PassportsState extends State<PassportsScreen> {
  List<String> paths = List();


  @override
  Widget build(BuildContext context) {
    Future<List<String>> photosPathsFuture = _getPassportsPaths();
    photosPathsFuture.then((List<String> photosPaths) {
      if (photosPaths.length != paths.length) {
        setState(() {
          paths = photosPaths;
        });
      }
    });
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Паспорта',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: _getGridView(paths, context),
    );
  }

  /// creates an grid view to be displayed on screen
  /// consists of images and button
  Widget _getGridView(List<String> photosPaths, BuildContext context) {
    List<Widget> children = _getPhotosViews(photosPaths, context);
    children.add(_getNewPhotoButton());
    return GridView.count(
      padding: EdgeInsets.all(16.0),
      crossAxisCount: 2,
      children: children,
      mainAxisSpacing: 8.0,
      crossAxisSpacing: 8.0,
    );
  }

  /// returns list of images in passports directory
  List<Widget> _getPhotosViews(List<String> photosPaths, BuildContext context) {
    List<Widget> result = List();
    double radius = 8.0;
    for (int i = 0; i < photosPaths.length; i++) {
      File file = File(photosPaths[i]);
      result.add(GestureDetector(
        onTap: () => _openImage(context, photosPaths[i]),
        onLongPress: () => _deleteImage(context, photosPaths[i]),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(radius),
            image: DecorationImage(
              fit: BoxFit.cover,
              image: FileImage(file),
            ),
          ),
        ),
      ));
    }

    return result;
  }

  /// returns a list of URL to the passports files inside
  /// '<application documents directory>/passports/'
  Future<List<String>> _getPassportsPaths() async {
    Directory docsDir;
    try {
      docsDir = await getApplicationDocumentsDirectory();
      print(docsDir.path);
    } catch (e) {
      // TODO: handle error, low probability actually
      debugPrint(
          'ERROR getting application\' docs directory: \n' + e.toString());
      return null;
    }

    String passPath = docsDir.path + '/passports';

    Directory passDir;
    try {
      passDir = Directory(passPath);
      bool exists = await passDir.exists();
      if (!exists) {
        // if directory doesn't exists -> create it
        await passDir.create(recursive: true);
      }
    } catch (e) {
      debugPrint('ERROR opening passports directory: \n' + e.toString());
      return null;
    }

    List<String> result = List();
    try {
      List<FileSystemEntity> paths = await passDir.list().toList();
      for (int i = 0; i < paths.length; i++) {
        if (_isPassportFile(paths[i].path)) {
          result.add(paths[i].path);
        }
      }
    } catch (e) {
      debugPrint('ERROR getting list of files in directory: \n' + e.toString());
      return null;
    }
    return result;
  }

  /// checks if path is an path to the passports file
  /// actually checks that last part after separator matches regular expressions
  /// 'passport_[0-9][0-9]*\\.jpg'
  bool _isPassportFile(path) {
    String name = basename(path);
    RegExp regExp = RegExp('passport_[0-9][0-9]*\\.jpg');
    return regExp.hasMatch(name);
  }

  /// creates a button
  /// clicking this button will open an camera
  /// and store photo to in the '<application documents directory>/passports/' directory
  Widget _getNewPhotoButton() {
    return Container(
        decoration: BoxDecoration(
            color: ColorsConfig.dodgerBlue,
            borderRadius: BorderRadius.all(Radius.circular(16.0))),
          child: Stack(
            children: <Widget>[
              Positioned(
                top: 16.0,
                right: 16.0,
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                  size: iconSize,
                ),
              ),
              Positioned(
                bottom: 16.0,
                left: 16.0,
                right: 16.0,
                child: Text(
                  'Сделать фото паспорта',
                  style: TextStyles.label_large.copyWith(color: Colors.white),
                ),
              ),
              GestureDetector(onTap: _makePhoto,),
            ],
          ),
        );
  }

  /// take a photo and store it
  Future _makePhoto() async {
    // For now image picker saves files to his default directory,
    // I didn't find a reliable way to access this directory
    // so I copy image to my directory and delete old image
    // may be ability to save into custom directory will appear later
    // TODO: redo this shame
    print('tap');
    File image =
    await ImagePicker.pickImage(source: ImageSource.camera); // take photo
    if (image == null)
      return;
    File newFile =
    await _getNewImageFile(); // create a file for storing a photo
    newFile.writeAsBytesSync(image.readAsBytesSync()); // copy image to new file
    image.delete(); // delete old image file
    List<String> photosPaths =
    await _getPassportsPaths(); // refresh list of photos
    setState(() {
      // refresh state
      paths = photosPaths;
    });
  }

  /// creates a new file to store an image
  /// '<application documents directory>/passports/passport_<current time milliseconds>.jpg'
  Future<File> _getNewImageFile() async {
    Directory docsDir = await getApplicationDocumentsDirectory();
    DateTime now = DateTime.now();
    String passPath = docsDir.path +
        '/passports/passport_' +
        now.millisecondsSinceEpoch.toString() +
        '.jpg';
    File passFile = File(passPath);
    await passFile.create(recursive: true);
    return passFile;
  }

  _openImage(BuildContext context, String photosPath) {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ImageScreen(
            path: photosPath,
          ),
        ));

  }

  /// deletes an image and refreshes state after
  _deleteImage(BuildContext context, String photoPath) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // user must not tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Удалить фото?'),
          actions: <Widget>[
            FlatButton(
              child: Text('Удалить'),
              onPressed: () {
                File file = File(photoPath);
                file.delete();
                Future<List<String>> photosPaths = _getPassportsPaths(); // refresh list of photos
                photosPaths.then((List<String> photoPaths) {
                  setState(() {
                    paths = photoPaths;
                  });
                });
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Не-а'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
