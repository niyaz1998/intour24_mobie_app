import 'package:flutter/material.dart';

import '../model/chat_message.dart';
import '../widgets/chat_message.dart';

class ChatScreen extends StatefulWidget {
  @override
  ChatScreenState createState() => ChatScreenState();
}

class ChatScreenState extends State<ChatScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: _getAppBar(),
        body: Column(children: [
          Expanded(
            child: _getMessages(),
          ),
          SizedBox(
            height: 8.0,
          ),
          _getEnterMessageField(),
        ]));
  }

  Widget _getMessages() {
    if (data.length == 0) {
      return ListView(
        children: <Widget>[],
      );
    } else {
      List<Widget> res = List();
      res.add(ChatMessageWidget(message: data[0]));
      double height;
      for (int i = 1; i < data.length; i++) {
        // if sender of messages changes - distance between message is 16.0
        // if sender remains same - distance is 8.0
        if (data[i - 1].sender == data[i].sender) {
          height = 8.0;
        } else {
          height = 16.0;
        }

        res.add(SizedBox(
          height: height,
        ));
        res.add(ChatMessageWidget(message: data[i]));
      }

      res.add(SizedBox(
        height: 8.0,
      ));

      return ListView.builder(
          itemCount: res.length,
          itemBuilder: (context, index) {
            return Container(
              child: res[index],
              padding: EdgeInsets.only(right: 16.0, left: 16.0),
            );
          });
    }
  }

  Widget _getAppBar() {
    return AppBar(
      title: Text('Чат'),
      actions: <Widget>[
        IconButton(icon: Icon(Icons.refresh), onPressed: () {}),
        IconButton(
          icon: Icon(Icons.thumbs_up_down),
          onPressed: () {},
        )
      ],
    );
  }

  Widget _getEnterMessageField() {
    return Container(
        padding: EdgeInsets.only(right: 16.0, left: 16.0),
        child: Row(
          children: <Widget>[
            Expanded(
              child: TextField(
                decoration: InputDecoration(hintText: 'Введите сообщение'),
                autocorrect: true,
              ),
            ),
            IconButton(
              icon: Icon(Icons.send),
              onPressed: () {},
            )
          ],
        ));
  }
}
