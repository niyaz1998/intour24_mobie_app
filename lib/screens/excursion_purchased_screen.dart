import 'package:flutter/material.dart';


/// screen that tells that excursion was successfully purchased
class PurchasedScreen extends StatelessWidget {
  final String text;

  PurchasedScreen(this.text);

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Center(child: Text(text),));
  }}