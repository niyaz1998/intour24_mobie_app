import 'package:flutter/material.dart';
import '../config/colors_config.dart';
import '../model/excursion.dart';
import '../config/text_styles_config.dart';
import '../config/icons_config.dart';

/// screen with list of purchased excursions
class PurchasedExcursionsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Приобретенные экскурсии'),),
      body: ListView.builder(
          padding: EdgeInsets.all(8.0),
          itemCount: purchasedExcursion.length,
          itemBuilder: (BuildContext context, int index) {
            return PurchasedExcursionWidget(purchasedExcursion[index]);
          }),
    );
  }
}

class PurchasedExcursionWidget extends StatelessWidget {
  final PurchasedExcursion purchasedExcursion;

  PurchasedExcursionWidget(this.purchasedExcursion);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(8.0),
        child: Material(
            elevation: 8.0,
            child: Container(
              padding: EdgeInsets.all(8.0),
              color: ColorsConfig.silverSand,
              child: Column(
                children: <Widget>[
                  Row(
                    children: [
                      Expanded(
                          child: Text(
                        purchasedExcursion.excursion.titleOriginal,
                        style: TextStyles.text_large,
                      )),
                      Text(
                        _calculateTotalSum(),
                        style: TextStyles.text_large,
                      )
                    ],
                    crossAxisAlignment: CrossAxisAlignment.start,
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.event,
                        size: iconSize,
                      ),
                      Text(purchasedExcursion.date +
                          ' ' +
                          purchasedExcursion.excursion.startTime +
                          '-' +
                          purchasedExcursion.excursion.endTime)
                    ],
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.accessibility,
                        size: iconSize,
                      ),
                      Expanded(
                          child: Text(
                        purchasedExcursion.adultTickets.toString(),
                        style: TextStyles.text_large,
                      )),
                      Icon(
                        Icons.child_care,
                        size: iconSize,
                      ),
                      Expanded(
                          child: Text(
                        purchasedExcursion.childTickets.toString(),
                        style: TextStyles.text_large,
                      )),
                    ],
                  ),
                ],
              ),
            )));
  }

  String _calculateTotalSum() {
    return (purchasedExcursion.excursion.getChildPrice() *
                    purchasedExcursion.childTickets +
                purchasedExcursion.excursion.getAdultPrice() *
                    purchasedExcursion.adultTickets)
            .toString() +
        ' ' +
        purchasedExcursion.excursion.getCurrency();
  }
}
