import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import '../config/colors_config.dart';
import '../config/text_styles_config.dart';

/// page to login using phone number
class PhoneLoginForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => PhoneLoginState();
}

/// page to login into the system using phone
class PhoneLoginState extends State {
  final _formKey = GlobalKey<FormState>();

  String getNumbersInMask(String value, String mask) {
    String res = '';
    for (int i = 0; i < value.length; i++) {
      if (mask[i] == '0') {
        res += value[i];
      }
    }
    return res;
  }

  @override
  Widget build(BuildContext context) {
    String numberMask = '0(000) 000-00-00';
    var numberController = new MaskedTextController(mask: numberMask);
    TextFormField numberField = new TextFormField(
      validator: (value) {
        String number = getNumbersInMask(value, numberMask);
        if (number.length != 11) return 'Пожайлуста, введите номер телефона';
      }, // TextFormField.validator
      controller: numberController,
      style: TextStyles.h1,
      keyboardType: TextInputType.number,
    );

    return new Container(
        child: Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Введите свой номер телефона'),
          numberField,
          RaisedButton(
              child: Text('Отправить код', style: TextStyle(color: Colors.white)),
              color: ColorsConfig.flamingo,
              onPressed: () {
                String number =
                    getNumbersInMask(numberController.text, numberMask);
                Text res;
                if (number.length == 11) {
                  res = Text('Ждем кодик ;) на номер: ' + number);
                  sendGetCodeRequestToServer();
                } else {
                  res = Text('Пожайлуста, введите номер телефона',
                      style: TextStyle(color: Colors.red, fontSize: 15.0));
                }
                Scaffold.of(context).showSnackBar(SnackBar(content: res));
              }),
          TextFormField(
            validator: (value) {
              if (value.length != 6 || double.tryParse(value) == null)
                return 'Введите СМС код';
            },
            style: TextStyles.h1,
            keyboardType: TextInputType.number,
            maxLength: 6,
          ),
          RaisedButton(
            child: Text('Отправить', style: TextStyle(color: Colors.white)),
            color: ColorsConfig.flamingo,
            onPressed: () {
              if (_formKey.currentState.validate()) {
                // TODO: add checking if code is correct
                Navigator.pushNamed(context, '/');
              }
            },
          )
        ],
      ),
    ));
  }

  void sendGetCodeRequestToServer() {
    // TODO: implement
  }
}

class PhoneLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('СМС логин')),
      body: Container(
          padding: const EdgeInsets.all(40.0), child: PhoneLoginForm()),
    );
  }
}
