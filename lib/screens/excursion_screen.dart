import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:page_view_indicators/page_view_indicators.dart';

import '../config/colors_config.dart';
import '../config/icons_config.dart';
import '../config/text_styles_config.dart';
import '../model/excursion.dart';
import '../widgets/hinted_drop_down_button.dart';
import '../widgets/hinted_sum_drop_down_button.dart';
import 'excursion_purchased_screen.dart';

/// screen used to display detailed information about the excursion
///
/// WARNING: here is a page view that contains photos of excursions
/// I need to add a gradient, so white widgets on this photos will be visible
/// but adding gradient over page view disables gesture detection on page view
/// so i decided to add gradient on images itself, this will get work done,
/// but this approach is not effective enough

class ExcursionScreen extends StatefulWidget {
  final Excursion excursion;
  final String selectedDate;

  ExcursionScreen(this.excursion, this.selectedDate);

  @override
  State<StatefulWidget> createState() => _ExcursionScreenState();
}

class _ExcursionScreenState extends State<ExcursionScreen> {
  HintedSumDropDownButton _adultDropDownButton, _childDropDownButton;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(
      children: _getMainInfoList(context),
    ));
  }

  /// main function that forms list of main widgets
  ///
  /// padding from borders is equal to 16.0
  /// distance between elements differs
  List<Widget> _getMainInfoList(BuildContext context) {
    List<Widget> res = List();
    /// function to get code simpler
    void add(Widget w, double topPadding) {
      res.add(Container(
        child: w,
        padding: EdgeInsets.fromLTRB(16.0, topPadding, 16.0, 0.0),
      ));
    }

    res.add(_getPhotosPageView());
    add(_getTitleAndCost(), 16.0);
    add(_getExcursionDuration(), 16.0);
    add(_getExcursionCategory(), 8.0);
    add(_getServicesProvided(), 16.0);
    add(_getDescription(), 16.0);
    add(_getDeparturePlace(), 16.0);
    add(_getPurchaseSection(), 24.0);
    res.add(_getPurchaseButton());
    return res;
  }

  /// returns page view with excursion photos
  Widget _getPhotosPageView() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.width * 100 / 178;
    List<Widget> res = _getPhotosList(height, width);
    ValueNotifier<int> _currentPageNotifier = ValueNotifier<int>(0);

    return Container(
      child: Stack(
        children: <Widget>[
          PageView(
            controller: PageController(
                //viewportFraction: 0.9
                ),
            children: res,
            onPageChanged: (int index) {
              _currentPageNotifier.value = index;
            },
          ),
          Positioned(
            left: 0.0,
            right: 0.0,
            bottom: 0.0,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: CirclePageIndicator(
                selectedDotColor: Colors.white,
                itemCount: res.length,
                currentPageNotifier: _currentPageNotifier,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(16.0),
            child: IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  size: 32.0,
                  color: Colors.white,
                ),
                onPressed: () {
                  Navigator.pop(context);
                }),
          )
        ],
      ),
      width: width,
      height: height,
    );
  }

  Widget _getTitleAndCost() {
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
              child: Text(
            widget.excursion.titleOriginal,
            style: TextStyles.h1,
          )),
        ),
        Text(widget.excursion.cost, style: TextStyles.h2)
      ],
    );
  }

  Widget _getExcursionDuration() {
    return Wrap(
      children: <Widget>[
        Icon(
          Icons.access_time,
          size: iconSize,
          color: Colors.black,
        ),
        SizedBox(
          width: 8.0,
        ),
        Text(
          widget.excursion.startTime +
              ' - ' +
              widget.excursion.endTime +
              ' ' +
              widget.excursion.duration,
          style: TextStyles.text_large,
        )
      ],
    );
  }

  Widget _getExcursionCategory() {
    return Wrap(
      children: <Widget>[
        Icon(
          ExcursionsCategoriesIcons.getIcon(widget.excursion.type),
          size: iconSize,
        ),
        SizedBox(
          width: 8.0,
        ),
        Text(
          ExcursionsCategoriesIcons.getString(widget.excursion.type),
          style: TextStyles.text_large,
        )
      ],
    );
  }

  Widget _getServicesProvided() {
    List<Widget> temp = List();
    List<String> codes = widget.excursion.getCodes();
    temp.add(
      Text(
        'Предоставляемые сервисы: ',
        style: TextStyles.h2,
      ),
    );
    for (int i = 0; i < codes.length; i++) {
      temp.add(Container(
        child: Row(
          children: <Widget>[
            Icon(
              ExcursionServicesIcons.getIcon(codes[i]),
              size: iconSize,
            ),
            SizedBox(
              height: 8.0,
              width: 8.0,
            ),
            Text(
              ExcursionServicesIcons.getString(codes[i]),
              style: TextStyles.text_large,
            )
          ],
        ),
        padding: EdgeInsets.only(top: 8.0),
      ));
    }
    return Column(
      children: temp,
      crossAxisAlignment: CrossAxisAlignment.start,
    );
  }

  Widget _getDescription() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Описание экскурсии',
          style: TextStyles.h2,
        ),
        Text(
          widget.excursion.description,
          style: TextStyles.text_xSmall,
        )
      ],
    );
  }

  Widget _getDeparturePlace() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Место отправления',
          style: TextStyles.h2,
        ),
        SizedBox(
          height: 4.0,
        ),
        Text(
          widget.excursion.departurePlace,
          style: TextStyles.text_xSmall,
        )
      ],
    );
  }

  Widget _getPurchaseSection() {
    double width = (MediaQuery.of(context).size.width - 24 - 32) / 2;
    _adultDropDownButton = HintedSumDropDownButton(
        widget.excursion.getAdultPrice(),
        'Взрослые',
        widget.excursion.getCurrency(),
        width);
    _childDropDownButton = HintedSumDropDownButton(
        widget.excursion.getChildPrice(),
        'Дети',
        widget.excursion.getCurrency(),
        width);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Данные бронирования',
          style: TextStyles.h1,
        ),
        SizedBox(
          height: 8.0,
        ),
        HintedDropDownButton(
            widget.excursion.excursionDates, widget.selectedDate, 'Дата'),
        Row(
          children: <Widget>[
            _adultDropDownButton,
            SizedBox(
              width: 24.0,
            ),
            _childDropDownButton
          ],
        )
      ],
    );
  }

  List<Widget> _getPhotosList(double height, double width) {
    List<Widget> res = List();
    for (int i = 0; i < widget.excursion.photosLink.length; i++) {
      res.add(Container(
          height: height,
          width: width,
          child: Stack(
            children: <Widget>[
              Center(child: CachedNetworkImage(
                imageUrl: widget.excursion.photosLink[i],
                placeholder: Icon(Icons.cloud_download, size: height - 16.0,),
                fit: BoxFit.cover,
                width: width,
                height: height,
              )),
              Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: FractionalOffset.topCenter,
                        end: FractionalOffset.center,
                        colors: ColorsConfig.gradientColors,
                        stops: [0.0, 1.0])),
              ),
              Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: FractionalOffset.bottomCenter,
                        end: FractionalOffset.center,
                        colors: ColorsConfig.gradientColors,
                        stops: [0.0, 1.0])),
              ),
            ],
          )));
    }
    return res;
  }

  Widget _getPurchaseButton() {
    return FlatButton( // button to purchase an excursion
      onPressed: () {
        if (_adultDropDownButton.currentCost()!= 0 || _childDropDownButton.currentCost() !=0) {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      PurchasedScreen('adults cost: ' +
                          _adultDropDownButton.currentCost().toString() +
                          '\nchildren cost: ' +
                          _childDropDownButton.currentCost().toString())));
        }
      },
      child: Container(
        child: Text(
          'ЗАБРОНИРОВАТЬ',
          style: TextStyles.label_large,
        ),
        padding: EdgeInsets.all(16.0),
      ),
      color: ColorsConfig.flamingo,
      textColor: Colors.white,
    );
  }
}
