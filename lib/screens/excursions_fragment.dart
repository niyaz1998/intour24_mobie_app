import 'package:flutter/material.dart';
import '../managers/excursion_manager.dart';
import '../model/excursion.dart';
import '../widgets/excursion_widget.dart';
import '../config/text_styles_config.dart';

/// Screen that allows to user fast look on the main information about excursion
///
/// app bar allows to navigate between dates and also have button to see purchased excursion
/// body contains list of excursions presented by [ExcursionWidget] file
/// by clicking on the excursion it moves to the screen with detailed information about the excursion
class ExcursionFragment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ExcursionFragmentState();
}

class ExcursionFragmentState extends State<ExcursionFragment> {
  ExcursionManager excursionManager;
  String _currentDate;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _getAppBar(),
      body: ListView.builder(
          padding: EdgeInsets.all(8.0),
          itemCount: excursionManager.getCurrentExcursions().length,
          itemBuilder: (BuildContext context, int index) {
            return ExcursionWidget(
                excursionManager.getCurrentExcursions()[index], _currentDate);
          }),
    );
  }

  ExcursionFragmentState() {
    excursionManager = ExcursionManager(excursionPreList);
    _currentDate = excursionManager.getCurrentDate();
  }

  // moving to the next date in the excursion manager
  void _onLeftPressed() {
    if (excursionManager.hasPreviousDate()) {
      excursionManager.moveToPreviousDate();
      setState(() {
        _currentDate = excursionManager.getCurrentDate();
      });
    }
  }

  // moving to previous date in the excursion manager
  void _onRightPressed() {
    if (excursionManager.hasNextDate()) {
      excursionManager.moveToNextDate();
      setState(() {
        _currentDate = excursionManager.getCurrentDate();
      });
    }
  }

  /// contains:
  /// icon button 'left' to move to previous date
  /// text button that shows current date (in future will open an date picker)
  /// icon button 'right' to move to next date
  /// icon button 'shop cart' to move to screen with purchased excursion
  AppBar _getAppBar() {
    return AppBar(
      title: Row(
        children: <Widget>[
          IconButton(
              icon: Icon(Icons.keyboard_arrow_left), onPressed: _onLeftPressed),
          FlatButton(
            child: Text(
              _currentDate,
              style: TextStyles.text_large.copyWith(color: Colors.white),
            ),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.keyboard_arrow_right),
            onPressed: _onRightPressed,
          )
        ],
      ),
      centerTitle: true,
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.shopping_cart),
          onPressed: () {
            Navigator.pushNamed(context, '/purchased_excursions');
          },
        )
      ],
    );
  }
}
