import 'package:flutter/material.dart';
import '../managers/main_manager.dart';
import 'documents_fragment.dart';
import '../config/colors_config.dart';
import 'help_fragment.dart';
import 'excursions_fragment.dart';

/// allows to navigate between 3 main fragments (don't look for big meaning in this word)
/// documents fragment - contains main information
/// help fragment - questions and contacts with support
/// excursion fragment - list of available excursions
class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 1;

  @override
  Widget build(BuildContext context) {
    // TODO: make onBackPressed closing an app

    _loginCheck();

    return Scaffold(
      body: Center(
        child: _getBody(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.chat_bubble), title: Text('Помощь')),
          BottomNavigationBarItem(icon: Icon(Icons.description), title: Text('Документы')),
          BottomNavigationBarItem(icon: Icon(Icons.collections_bookmark), title: Text('Экскурсии')),
        ],
        currentIndex: _selectedIndex,
        fixedColor: ColorsConfig.flamingo,
        onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  void _loginCheck() {
    MainManager manager = new MainManager();

    if (! manager.isLogged()) {
      // TODO: implement "wait data downloading"
    }
  }

  Widget _getBody(int i) {
    switch (i) {
      case 0:
        return HelpScreen();
      case 1:
        return DocumentsScreen();
      case 2:
        return ExcursionFragment();
    }
    return DocumentsScreen();
  }
}