import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'dart:io';

/// this screen shows image specified in the path variable
class ImageScreen extends StatelessWidget {

  final String path;


  ImageScreen({Key key, @required this.path}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: PhotoView(
          imageProvider: FileImage(File(path)),
        )
    );
  }

}